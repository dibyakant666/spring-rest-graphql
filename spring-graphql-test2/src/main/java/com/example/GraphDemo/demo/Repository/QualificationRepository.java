package com.example.GraphDemo.demo.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.example.GraphDemo.demo.Entity.Qualification;


public interface QualificationRepository extends JpaRepository<Qualification, Integer>{


}
