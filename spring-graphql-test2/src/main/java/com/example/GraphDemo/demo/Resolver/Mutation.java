package com.example.GraphDemo.demo.Resolver;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.example.GraphDemo.demo.Entity.Address;
import com.example.GraphDemo.demo.Entity.Qualification;
import com.example.GraphDemo.demo.Entity.User;
import com.example.GraphDemo.demo.Repository.AddressRepository;
import com.example.GraphDemo.demo.Repository.QualificationRepository;
import com.example.GraphDemo.demo.Repository.UserRepository;

import javassist.NotFoundException;

@Component
public class Mutation implements GraphQLMutationResolver {
	private UserRepository uRepo;
	private AddressRepository aRepo;
	private QualificationRepository qRepo;
	
	@Autowired
	public Mutation(UserRepository uRepo, AddressRepository aRepo, QualificationRepository qRepo) {
		
		this.uRepo = uRepo;
		this.aRepo = aRepo;
		this.qRepo = qRepo;
	}
	public Address createAddress(String street,String city,String state,String country,Integer zip) {
		Address address=new Address();
		address.setCity(city);
		address.setCountry(country);
		address.setStreet(street);
		address.setZip(zip);
		address.setState(state);
		aRepo.save(address);
		return address;
	}
	public Qualification createQualification(String qdetails){
		Qualification qualification=new Qualification();
		qualification.setQdetails(qdetails);
		qRepo.save(qualification);
		return qualification;
		}
	public User createUser(String name,Integer qualification_id,Integer address_id) {
		User user=new User();
		user.setName(name);
		user.setAddress(new Address(address_id));
		user.setQualification(new Qualification(qualification_id));
		uRepo.save(user);
		return user;
	}
	public User updateUser(Integer id, String name) throws NotFoundException{
		Optional<User> upUser = uRepo.findById(id);
		
		if(upUser.isPresent()) {
			User user=upUser.get();
			
			if(name!=null)
				user.setName(name);
			uRepo.save(user);
			return user;
		}
		throw new NotFoundException("User ID "+id+" not found");
	}
	public boolean deleteUser(Integer id) {
		uRepo.deleteById(id);
		aRepo.deleteById(id);
		qRepo.deleteById(id);
		return true;
	}
}
