package com.example.GraphDemo.demo.Resolver;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example.GraphDemo.demo.Entity.Address;
import com.example.GraphDemo.demo.Entity.Qualification;
import com.example.GraphDemo.demo.Entity.User;
import com.example.GraphDemo.demo.Repository.AddressRepository;
import com.example.GraphDemo.demo.Repository.QualificationRepository;
import com.example.GraphDemo.demo.Repository.UserRepository;
@Component
public class Query implements GraphQLQueryResolver {
	private UserRepository uRepo;
	private AddressRepository aRepo;
	private QualificationRepository qRepo;
	@Autowired
    public Query(UserRepository uRepo, AddressRepository aRepo, QualificationRepository qRepo) {
		this.uRepo = uRepo;
		this.aRepo = aRepo;
		this.qRepo = qRepo;
	}
	public Iterable<User> findAllUser(){
		return uRepo.findAll();
	}
	public Iterable<Address> findAllAddress(){
		return aRepo.findAll();
	}
	public Iterable<Qualification> findAllQualification(){
		return qRepo.findAll();
	}
}
