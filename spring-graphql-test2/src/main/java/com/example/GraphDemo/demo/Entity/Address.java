package com.example.GraphDemo.demo.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
@Entity
public class Address {
	@Id
	@SequenceGenerator(
			name="address_seq",
			sequenceName="address_seq",
			allocationSize=1
			)
			@GeneratedValue(
			strategy=GenerationType.SEQUENCE,
			generator="address_seq"
			)
	private Integer aid;
	private String street;
	private String city;
	private String state;
	private String country;
	private Integer zip;
	
	
	public Address(Integer aid) {
	
		this.aid = aid;
	}
	public Address() {

	}
	public Integer getaId() {
		return aid;
	}
	public void setaId(Integer aId) {
		this.aid = aId;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getZip() {
		return zip;
	}
	public void setZip(Integer zip) {
		this.zip = zip;
	}
	public Address(String street, String city, String state, String country, Integer zip) {
		
		this.street = street;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zip = zip;
	}
	@Override
	public String toString() {
		return "Address [aid=" + aid + ", street=" + street + ", city=" + city + ", state=" + state + ", country="
				+ country + ", zip=" + zip + "]";
	}
	
	
}
