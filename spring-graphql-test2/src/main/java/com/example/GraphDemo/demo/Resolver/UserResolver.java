package com.example.GraphDemo.demo.Resolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.coxautodev.graphql.tools.GraphQLResolver;
import com.example.GraphDemo.demo.Entity.Address;
import com.example.GraphDemo.demo.Entity.Qualification;
import com.example.GraphDemo.demo.Entity.User;
import com.example.GraphDemo.demo.Repository.AddressRepository;
import com.example.GraphDemo.demo.Repository.QualificationRepository;

@Component 
public class UserResolver implements GraphQLResolver<User>{
	@Autowired
	private QualificationRepository qRepo;
	private AddressRepository aRepo;
	public UserResolver(AddressRepository aRepo,QualificationRepository qRepo) {
		this.qRepo=qRepo;
		this.aRepo=aRepo;
	}
	public Address getAddress(User user) {
		return aRepo.findById(user.getAddress().getaId()).orElseThrow(null);
	}
	public Qualification getQualification(User user) {
		return qRepo.findById(user.getQualification().getQid()).orElseThrow(null);
	}
}
