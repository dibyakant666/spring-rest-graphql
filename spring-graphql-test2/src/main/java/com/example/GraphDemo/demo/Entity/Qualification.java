package com.example.GraphDemo.demo.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
@Entity
public class Qualification {
	@Id
	@SequenceGenerator(
			name="qualification_seq",
			sequenceName="qualification_seq",
			allocationSize=1
			)
			@GeneratedValue(
			strategy=GenerationType.SEQUENCE,
			generator="qualification_seq"
			)
private Integer qid;
private String qdetails;
public Integer getQid() {
	return qid;
}
public void setQid(Integer qid) {
	this.qid = qid;
}
public String getQdetails() {
	return qdetails;
}
public void setQdetails(String qdetails) {
	this.qdetails = qdetails;
}
public Qualification(Integer qid) {

	this.qid = qid;
}
public Qualification(String qdetails) {
	
	this.qdetails = qdetails;
}
public Qualification() {
	// TODO Auto-generated constructor stub
}
public Qualification(Integer qid, String qdetails) {
	
	this.qid = qid;
	this.qdetails = qdetails;
}
@Override
public String toString() {
	return "Qualification [qid=" + qid + ", qdetails=" + qdetails + "]";
}


}
