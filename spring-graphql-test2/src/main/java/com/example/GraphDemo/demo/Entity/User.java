package com.example.GraphDemo.demo.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;


@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	@OneToOne(cascade = CascadeType.MERGE)
	@JoinColumn(
	name="qualification_id",
	referencedColumnName="qid"
	)
	private Qualification qualification;
	@OneToOne(cascade = CascadeType.MERGE)
	@JoinColumn(
	name="address_id",
	referencedColumnName="aid"
	)
	private Address address;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Qualification getQualification() {
		return qualification;
	}
	public void setQualification(Qualification qualification) {
		this.qualification = qualification;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public User(String name, Qualification qualification, Address address) {
		
		this.name = name;
		this.qualification=qualification;
		this.address = address;
	}
	public User() {
		
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", qid=" + qualification + ", aid=" + address + "]";
	}
	

}
