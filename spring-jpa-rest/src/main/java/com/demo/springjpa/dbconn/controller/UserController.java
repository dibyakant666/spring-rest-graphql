package com.demo.springjpa.dbconn.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.demo.springjpa.dbconn.entity.User;
import com.demo.springjpa.dbconn.service.UserService;


@RestController

public class UserController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserService service;
	
	@PostMapping("/adduser")
	public User addUser(@RequestBody User user) {
		LOGGER.info(user.getName());
		
		return service.saveUser(user);
	}
	@PostMapping("/addusers")
	public List<User> addUsers(@RequestBody List<User> users) {
		return service.saveUsers(users);
	}
	@GetMapping("/users")
	public List<User> findAllUsers(){
		return service.getUsers();
	}
	@GetMapping("/user/{id}")
	public User findUserById(@PathVariable int id) {
		return service.getUsersById(id);
	}
	@DeleteMapping("/delete/{id}")
	public String deleteUser(@PathVariable int id) {
		return service.deleteProduct(id);
	}
}
