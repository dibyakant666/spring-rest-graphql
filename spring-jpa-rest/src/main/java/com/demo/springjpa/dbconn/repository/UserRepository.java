package com.demo.springjpa.dbconn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.springjpa.dbconn.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

}
