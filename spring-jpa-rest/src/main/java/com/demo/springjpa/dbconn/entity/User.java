package com.demo.springjpa.dbconn.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
	
	@Id
	@SequenceGenerator(
			name="user_seq",
			sequenceName="user_seq",
			allocationSize=1
			)
	@GeneratedValue(
			strategy=GenerationType.SEQUENCE,
			generator="user_seq"
			)
	private int userId;
	private String name;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(
			name="address_id",
			referencedColumnName="addressId"
			)
	private Address address;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(
			name="qualification_id",
			referencedColumnName="qualId"
			)
	private Qualifications qualification;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Qualifications getQualification() {
		return qualification;
	}
	public void setQualification(Qualifications qualification) {
		this.qualification = qualification;
	}
	
}
